<?php
session_start();
session_destroy();
?>


<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <title>Logout</title>
</head>

<body class=" container-login">


  <style type="text/css">
    .container-login {
      width: 100%;
      min-height: 100vh;
      display: -webkit-flex;
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      align-items: center;
      padding: 250px;
      background: -webkit-linear-gradient(to right, #d35400, #c96a2a);
      background: linear-gradient(to right, #d35400, #c96a2a);
    }
  </style>
  <div class="container d-flex justify-content-center">
  <h2>GRACIAS POR CONFIAR EN TATAMOTOS!!!</h2>
  </div>

  <div class="container-fluid justify-content-center">

  </div>
</body>

</html>

<script>
  setTimeout("location.href = '../index.php';", 900);
</script>