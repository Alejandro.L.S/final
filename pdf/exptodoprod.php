<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
     $this->image('imagenes/images.jpeg',85,0,30);
    $this->Ln(10);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,10,'TataMotos ',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    
    $this->cell(18,10,'id_Producto',1,0,'c',0);
    $this->cell(25,10,'Codigo Barras',1,0,'c',0);
     $this->cell(35,10,'Descripcion',1,0,'c',0);
    $this->cell(25,10,'Detalle',1,0,'c',0);
        $this->cell(15,10,'Cantidad',1,0,'c',0);
            $this->cell(20,10,'Costo',1,0,'c',0);
                $this->cell(20,10,'Iva',1,0,'c',0);
                $this->cell(17,10,'Precio',1,0,'c',0);
                $this->cell(20,10,'Ubicacion',1,1,'c',0);
    
}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';

$consulta="SELECT * FROM producto_vista ";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

      $pdf->cell(18,8,$row['id_Producto'],1,0,'c',0);
    $pdf->cell(25,8,$row['Codigo_Barras'],1,0,'c',0);
     $pdf->cell(35,8,$row['Descripcion'],1,0,'c',0);
    $pdf->cell(25,8,$row['Detalle'],1,0,'c',0);
     $pdf->cell(15,8,$row['Cantidad'],1,0,'c',0);
      $pdf->cell(20,8,$row['Costo'],1,0,'c',0);
       $pdf->cell(20,8,$row['Iva'],1,0,'c',0);
   $pdf->cell(17,8,$row['Precio'],1,0,'c',0);
       $pdf->cell(20,8,$row['Ubicacion'],1,1,'c',0);
   
}

$pdf->Output();
?>

